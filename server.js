//require modules npm
const express = require("express");
const path = require("path");
const morgan = require('morgan');

//require modules app
const router = require('./routes/index');
//constantes
const app = express();
const port = process.env.PORT;

//use
app.use(morgan('tiny'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(router);
app.use(express.static(__dirname +'/node_modules/bootstrap/dist'));
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'ejs');







app.listen(port, () => console.log(`app listening at http://localhost:${port}`))