const mongoose = require('mongoose');
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
const URL = process.env.MONGO_URL;

//Connection with database
module.exports = {
    cnx: () => {
        console.log("Connection à la DB en cours ...");
        return mongoose.connect(URL,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            .then(() => console.log('Connexion à MongoDB réussie !'))
            .catch(() => console.log('Connexion à MongoDB échouée !'));
    },
    connection: mongoose.createConnection(URL),
}
   