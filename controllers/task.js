

const {
    findOneTask,
    createTask,
    findAllTasks,
    updateOneTask,
    deleteOneTask
} = require("../queries/Task.queries");

exports.getTask = async (_id) => {
    try {
        return await findOneTask(_id);
    } catch (error) {
        console.log(error)
    }
   
};
exports.getTasks = async () => {
    try {
        return await findAllTasks();
    } catch (error) {
        console.log(error)
    }
};
exports.addTask = async (data) => {
    try {
        return await createTask(data);
    } catch (error) {
        console.log(error);
    }
   
}
exports.updateTask = async (data)=>{
    return await updateOneTask(data);
}
exports.deleteTask = async _id => {
    return await deleteOneTask(_id);
}
exports.getDateFr = () => {
    let options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric" }
    return new Date().toLocaleDateString('fr-FR', options);
}