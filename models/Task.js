const mongoose = require("mongoose");
const schema = mongoose.Schema;
const CNX = require('../config/dbConfig').connection;
let options = { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric" }
 
const TaskSchema = new schema({
    title: String,
    description: String,
    done: { type: Boolean, default: false },
    createdAt: { type: String, default: new Date().toLocaleDateString('fr-FR', options)},
    updatedAt: { type: String, default: '' },
});

module.exports = CNX.model('tasks', TaskSchema);

