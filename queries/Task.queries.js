const Task = require("../models/Task");

exports.createTask = async (data) => {
    if (!data || !data.title || !data.description)
        throw new Error("Veuillez renseigner tous les champs requis svp");
    return await Task.create(data);
}


exports.findAllTasks = async () => {
    return await Task.find({}).exec();
}
exports.updateOneTask = async (data) => {
    return await Task.updateOne(
        {
            _id: data._id
        },
        {
            $set: data
        }
    )
}
exports.deleteOneTask = async _id => {
    try {
        return await Task.deleteOne({ _id }).exec();
    } catch (error) {
        console.log(error);
    }

}
exports.findOneTask = async _id => {
    try {
        return await Task.findOne({ _id });
    } catch (error) {
        console.log(error);
    }
}

