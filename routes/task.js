const router = require('express').Router();
const {
    getTask,
    addTask,
    getTasks,
    updateTask,
    deleteTask,
    getDateFr
} = require("../controllers/task");


//show all tasks
router.get('/', async (req, res) => {
    let todoList = await getTasks();
    console.log(todoList)
    res.render("todoList", {
        todoList,
    })
})

//show form to add or update task
router.get('/task-form', async (req, res) => {
    let task;
    if (req.query._id) {
        task = await getTask(req.query._id)
    }
    res.render("taskForm", {
        task: task ? task : {}
    });
})

//check and store task in DB
router.post('/add-task', async (req, res) => {
    let body = req.body
    //check data
    if (!body || !body.description || !body.title) {
        res.send('Vous devez renseinger tous les champs requis svp');
        res.end();
    }
    //update task
    if (body && body._id) {
        body.updatedAt = getDateFr();
        await updateTask(body);
        res.redirect('/');
    } else {//add new task
        delete req.body._id;
        //add Task to update
        try {
            await addTask(body);
            res.redirect("/");
        } catch (error) {
            res.send("Un probléme est survenu lors de l'ajout de la tâche");
            res.end();
        }



    }

});

//update status of task "done"
router.post('/task-done/:taskId/:bool', async (req, res) => {
    let data = {};
    data._id = req.params.taskId;
    data.done = req.params.bool;
    if (!data._id)
        res.json({
            msg: "taskId empty"
        })
    try {
        await updateTask(data);
      
        return res.json({
            msg: "success"
        })
    } catch (error) {
        return res.json({
            msg: "failure",
            error
        })
    }


})

//Delete task
router.delete('/delete-task/:taskId', async (req, res) => {
    let taskId = req.params.taskId
    if (!taskId)
        res.json({
            msg: "taskId empty"
        })
    try {
        await deleteTask(taskId);
        return res.json({
            msg: "success"
        })
    } catch (error) {
        return res.json({
            msg: "failure",
            error
        })
    }

})





module.exports = router;