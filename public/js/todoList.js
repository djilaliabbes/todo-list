function taskDone(taskId, bool) {
    let task = document.querySelector(`.task-${taskId}`);
    let btn = document.querySelector(`.btn-task-done-${taskId}`);
    fetch(`/task-done/${taskId}/${bool}`, {
        method: "POST",
    }).then(res => {
        if (res.ok) return res.json()
    }).then(res => {
        if (res.msg === "success") {
            btn.style.display = "none";
            task.classList.replace('list-group-item-danger','list-group-item-success')
        }
           
    }).catch(err => console.log(err));
}
const updateTask = (_id) => {
    location = `/task-form?_id=${_id}`
}
const deleteTask = (taskId,taskStatu) => {
console.log(taskStatu)
    if (!taskId)
        return console.log("Vous devez renseigner un taskId pour supprimer une tâche");
    if (taskStatu === false) {
       let result = confirm("cette tâche n'est pas terminée ou vient d'être validée, voulez vous vraiment la supprimer!");
        if (!result)
            return
    }
        
    let task = document.querySelector(`.task-${taskId}`);
    fetch(`/delete-task/${taskId}`, {
        method: "DELETE"
    }).then(res => {
        if (res.ok) return res.json()
    }).then(res => {
        console.log(res)
        if (res.msg === "success")
            task.remove()
    }).catch(err=>console.log(err))

}

